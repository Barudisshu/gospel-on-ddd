基础设施
====

# Cassandra docker 服务

```bash
docker pull cassandra
docker run --name cassandra -p 9042:9042 -d cassandra:latest --restart=always
```

# Elasticsearch

```bash
docker pull nshou/elasticsearch-kibana
docker run -d -p 9200:9200 -p 5601:5601 nshou/elasticsearch-kibana
```

# 初始化数据库

```bash
docker run -it --link cassandra:cassandra --rm cassandra cqlsh cassandra

```

```sql
CREATE KEYSPACE IF NOT EXISTS bookstore WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };

CREATE TABLE IF NOT EXISTS bookstore.projectionoffsets (identifier varchar primary key, offset bigint);
```

# CQRS架构原型

要设计一种响应式的Web服务，满足CAP理论；满足响应式宣言(Reactive Manifesto)的4个特征。尽管CQRS不是最好的实现，对于微服务架构还是不错的选择。

![CQRS](doc/CQRS.png)

# 领域驱动设计

参考 [这里](https://barudisshu.github.io/2017/11/13/pattern/ddd/dddfs/)
