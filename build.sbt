name := "gospel-on-ddd"

organization := "com.galudisu"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies ++= {
  val akkaVersion = "2.5.6"
  val akkaHttpVersion = "10.0.10"
  val slf4jVersion = "1.7.25"
  val log4j2Version = "2.9.0"
  val scalatestFullVersion = "3.0.3"
  val scalaMockVersion = "3.6.0"
  val elastic4sVersion = "5.6.0"

  Seq(
    // akka basic
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-remote" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
    "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,

    // akka persistence
    "com.typesafe.akka" %% "akka-persistence" % akkaVersion,

    // log
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "org.apache.logging.log4j" % "log4j-slf4j-impl" % log4j2Version,
    "org.apache.logging.log4j" % "log4j-api" % log4j2Version,
    "org.apache.logging.log4j" % "log4j-core" % log4j2Version,

    // test
    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpVersion % Test,
    "org.scalatest" %% "scalatest" % scalatestFullVersion,
    "org.scalamock" %% "scalamock-scalatest-support" % scalaMockVersion,

    // serialize
    "com.twitter" %% "chill-akka" % "0.9.2",

    // DI
    "org.scaldi" %% "scaldi-akka" % "0.5.8",

    // level db for local persistence
    "org.iq80.leveldb"            % "leveldb"          % "0.7",
    "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8",

    // elastic search
    "com.sksamuel.elastic4s" %% "elastic4s-core" % elastic4sVersion,
    "com.sksamuel.elastic4s" %% "elastic4s-http" % elastic4sVersion,
    "com.sksamuel.elastic4s" %% "elastic4s-spray-json" % elastic4sVersion,
    "com.sksamuel.elastic4s" %% "elastic4s-testkit" % elastic4sVersion % Test,
    "com.sksamuel.elastic4s" %% "elastic4s-embedded" % elastic4sVersion % Test

  )
}

// enable scala code formatting //
import com.typesafe.sbt.SbtScalariform

import scalariform.formatter.preferences._

// Scalariform settings
SbtScalariform.autoImport.scalariformPreferences :=
  SbtScalariform.autoImport.scalariformPreferences.value
  .setPreference(AlignSingleLineCaseStatements, true)
  .setPreference(AlignSingleLineCaseStatements.MaxArrowIndent, 100)
  .setPreference(DoubleIndentConstructorArguments, true)
  .setPreference(RewriteArrowSymbols, true)

// enable updating file headers //
import de.heikoseeberger.sbtheader.license.Apache2_0

//headers := Map(
//  "scala" -> Apache2_0("2016", "Galudisu"),
//  "conf" -> Apache2_0("2016", "Galudisu", "#")
//)

enablePlugins(AutomateHeaderPlugin)

scalacOptions in ThisBuild ++= Seq("-unchecked", "-deprecation")

lazy val root = (project in file(".")).enablePlugins(JavaAppPackaging)

fork in run := true

        