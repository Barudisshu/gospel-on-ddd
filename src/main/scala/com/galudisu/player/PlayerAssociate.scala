package com.galudisu.player

import java.util.UUID

import akka.actor.{ActorRef, Props}
import com.galudisu.ddd.Aggregate
import com.galudisu.player.Player._
import com.galudisu.player.PlayerAssociate.CreateRequest

object PlayerAssociate {
  val Name = "player-associate"
  def props = Props[PlayerAssociate]

  case class CreateRequest(xp: Int, credits: Int)
}

class PlayerAssociate extends Aggregate[PlayerState, Player] {

  override def entityProps = Player.props

  override def receive: Receive = {
    case CreateRequest(xp, credits) ⇒
      forwardCommand(CreatePlayer(genId, xp, credits))
  }

  private def genId: String = UUID.randomUUID().toString
}
