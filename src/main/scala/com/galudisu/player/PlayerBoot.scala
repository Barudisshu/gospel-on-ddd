package com.galudisu.player

import akka.actor.ActorSystem
import com.galudisu.ddd.Bootstrap

class PlayerBoot extends Bootstrap{

  override def bootup(system: ActorSystem) = {
    import system.dispatcher
    val assoc = system.actorOf(PlayerAssociate.props, PlayerAssociate.Name)
    List(new PlayerRoutes(assoc))
  }
}
