package com.galudisu.player

import com.galudisu.ddd.Server

object Main {
  def main(args:Array[String]):Unit = {
    new Server(new PlayerBoot(), "player-processing")
  }
}