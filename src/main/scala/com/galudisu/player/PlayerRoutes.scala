package com.galudisu.player

import akka.actor.{ActorRef, ActorSystem}
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.stream.Materializer
import com.galudisu.ddd.BasicRoutesDefinition
import com.galudisu.player.PlayerAssociate.CreateRequest

import scala.concurrent.ExecutionContext

class PlayerRoutes(assoc: ActorRef)(implicit val ec: ExecutionContext) extends BasicRoutesDefinition {

  override def routes(implicit system: ActorSystem, ec: ExecutionContext, mater: Materializer): Route = {
    logRequestResult("server") {
      (get & path("player")) {
        serviceAndComplete[PlayerFO](CreateRequest(0, 0), assoc)
      }
    }

  }
}
