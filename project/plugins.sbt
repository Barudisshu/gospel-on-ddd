resolvers += "sonatype-releases" at "https://oss.sonatype.org/content/repositories/releases/"

// to format scala source code
addSbtPlugin("org.scalariform" % "sbt-scalariform" % "1.8.2")

// enable updating file headers eg. for copyright
addSbtPlugin("de.heikoseeberger" % "sbt-header" % "1.5.1")

// sbt native package
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.1")
